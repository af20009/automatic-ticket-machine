import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class Food {

    int value1 = 500;
    int value2 = 510;
    int value3 = 520;
    int value4 = 530;
    int value5 = 540;
    int value6 = 550;
    int value7 = 150;
    int value8 = 160;
    int value9 = 170;
    int value10 = 180;
    int value11 = 190;
    int value12 = 200;
    int sum = 0;
    static double place = 1;

    List<String> list = new ArrayList<String>();
    String selectFoodValues[] = {"S", "M", "M-extra", "L", "XL", "MEGA"};
    String selectDrinkValues[] = {"S", "M", "L"};

    public JFrame frame = new JFrame();
    private JPanel root;
    private JLabel topLabel1;
    private JButton food1;
    private JButton food2;
    private JButton food3;
    private JButton food4;
    private JButton food5;
    private JButton food6;
    private JButton checkOutButton1;
    private JTextPane textPane1;
    private JButton reset1;
    private JLabel ordertext1;
    private JLabel Total;
    private JTabbedPane tabbedPane1;
    private JButton drink1;
    private JButton drink2;
    private JButton drink3;
    private JButton drink4;
    private JButton drink5;
    private JButton drink6;
    private JButton reset2;
    private JTextPane textPane2;
    private JButton checkOutButton2;
    private JPanel root1;
    private JPanel root2;
    private JLabel topLabel2;
    private JLabel ordertext2;
    private JLabel total2;

    //はい、いいえのアクションリスナー
    public int yesNo(String message, String title){
        int select = JOptionPane.showConfirmDialog(null,
                message,
                title,
                JOptionPane.YES_NO_OPTION);

        return select;
    }

    //それぞれのメニューを選んだときの処理
    public void selectSize(int value, String type, String selectvalues[]) {
        //サイズ選択
        int selectSize = JOptionPane.showOptionDialog(frame,
                "What size do you want?",
                "Size",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                selectvalues,
                selectvalues[0]);

        if (selectSize != -1) {
            //選んだ牛丼の確認
            int select = yesNo("Would you like to order " + type + " (" + selectvalues[selectSize] + ") ?", "Order Confimation");

            //確認出来たら
            if(select == 0) {
                //メッセージダイアログで選んだ商品を表示
                JOptionPane.showMessageDialog(null,
                        "Thank you for ordering " + type + " (" + selectvalues[selectSize] + ")! It will be served as soon as possible.");

                //選んだサイズによって値段を決定
                int val = (int) ((value + selectSize * 50) * place);

                sum += val;
                Total.setText(sum + " yen");
                total2.setText(sum + " yen");
                list.add(type + " (" + selectvalues[selectSize] + ") " + val + "yen\n");
                String str = "";

                for (int i = 0; i < list.size(); i++) {
                    str += list.get(i);
                }
                textPane1.setText(str);
                textPane2.setText(str);
            }
        }
    }

    //注文をリセット
    public void clear(){
        int num = list.size();
        sum = 0;
        Total.setText("0 yen");
        total2.setText("0 yen");
        textPane1.setText("");
        textPane2.setText("");
        for(int i = num-1; i >= 0; i--) {
            list.remove(i);
        }
    }

    public Food() {
        ImageIcon gyudon1 = new ImageIcon("src/photo/gyudon1.png");
        food1.setIcon(gyudon1);
        ImageIcon gyudon2 = new ImageIcon("src/photo/gyudon2.png");
        food2.setIcon(gyudon2);
        ImageIcon gyudon3 = new ImageIcon("src/photo/gyudon3.png");
        food3.setIcon(gyudon3);
        ImageIcon gyudon4 = new ImageIcon("src/photo/gyudon4.png");
        food4.setIcon(gyudon4);
        ImageIcon gyudon5 = new ImageIcon("src/photo/gyudon5.png");
        food5.setIcon(gyudon5);
        ImageIcon gyudon6 = new ImageIcon("src/photo/gyudon6.png");
        food6.setIcon(gyudon6);

        ImageIcon softD1 = new ImageIcon("src/photo/drink1.png");
        drink1.setIcon(softD1);
        ImageIcon softD2 = new ImageIcon("src/photo/drink2.png");
        drink2.setIcon(softD2);
        ImageIcon softD3 = new ImageIcon("src/photo/drink3.png");
        drink3.setIcon(softD3);
        ImageIcon softD4 = new ImageIcon("src/photo/drink4.png");
        drink4.setIcon(softD4);
        ImageIcon softD5 = new ImageIcon("src/photo/drink5.png");
        drink5.setIcon(softD5);
        ImageIcon softD6 = new ImageIcon("src/photo/drink6.png");
        drink6.setIcon(softD6);

        tabbedPane1.setTitleAt(0, "Food");
        tabbedPane1.setTitleAt(1, "Drink");

        food1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value1, "Gyudon", selectFoodValues);
            }
        });
        food2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value2, "Gyudon with kimuchi", selectFoodValues);
            }
        });
        food3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value3, "Gyudon with green onion and raw egg", selectFoodValues);
            }
        });
        food4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value4, "Gyudon with 3 cheeses", selectFoodValues);
            }
        });
        food5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value5, "Gyudon with grated radish", selectFoodValues);
            }
        });
        food6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value6, "Gyudon with mustard leaf, cod caviar and mayonnaise", selectFoodValues);
            }
        });
        reset1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clear();
            }
        });
        checkOutButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int select = yesNo(
                        "Would you like to checkout?",
                        "Checkout Confirmation");

                if(select == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + sum + "yen");

                    clear();
                }
            }
        });
        drink1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value7, "Coca-Cola", selectDrinkValues);
            }
        });
        drink2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value8, "Orange juice", selectDrinkValues);
            }
        });
        drink3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value9, "Melon soda", selectDrinkValues);
            }
        });
        drink4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value10, "Calpis", selectDrinkValues);
            }
        });
        drink5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value11, "Iced coffee", selectDrinkValues);
            }
        });
        drink6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectSize(value12, "Hot coffee", selectDrinkValues);
            }
        });
        reset2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clear();
            }
        });
        checkOutButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int select = yesNo(
                        "Would you like to checkout?",
                        "Checkout Confirmation");

                if(select == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + sum + "yen");

                    clear();
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Food");
        frame.setContentPane(new Food().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        //イートインかテイクアウトかを選択するダイアログを表示
        for(;;){
            String selectInOut[] = {"eatin", "takeout"};
            int select = JOptionPane.showOptionDialog(frame,
                    "Eatin or takeout?",
                    "Place",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    selectInOut,
                    selectInOut[0]);
            if(select != -1){
                switch (select){
                    case 0:
                        place *= 1.02;
                }
                break;
            }

        }
    }
}
